package bomberballagain;


	/*
	 * Nomenclature :
	 *  -3 : ennemis agressif 
	 *  -2 : ennemis actif
	 *  -1 : ennemis passif
	 * 	0  : mur incassable
	 *  1  : mur cassable
	 *  2  : bombe
	 *  3  : bonus port�e bombe
	 *  4  : bonus d�placement bombe
	 * */
	
public class Grid {	
	private MapObjects [][] grid;
	private int width;
	private int height;
	
	public Grid(int width, int height) {
		this.grid = new MapObjects[width][height]; //tableau de dimension 2 qui contient dans chaque case AGENNEMY, ACENNEMY, PENNEMY, UWALL, BWALL, BOMB, BRANGE, BMOOVE;
		this.setWidth(width);
		this.setHeight(height);
	}
	
	public MapObjects[][] getGrid (){return this.grid;}	
	public int getWidth() {return this.width;}
	public int getHeight() {return this.height;}
	
	public void setGrid(MapObjects[][] grid) {this.grid = grid;}
	public void setWidth(int width) {this.width = width;}
	public void setHeight(int height) {this.height = height;}
	
	
	public Grid setEmptyMap(int width, int height) {
		/* On prend width+2 et height+2 car on encercle la map de mur incassable
		 * Il est donc n�c�ssaire de prendre une marge pour avoir la bonne zone 
		 * disponible pass� en param�tre
		 * */
		Grid map = new Grid(width+2, height+2);
		MapObjects [][] tbl = map.getGrid();
		for (int i = 0 ; i < width+2 ; i++) {
			for (int j = 0 ; j < height+2 ; j++) {
				if (i == 0 || i == width+1 || j == 0 || j == height+1) {
					//On encercle la map de mur incassable
					tbl[i][j] = MapObjects.UWALL;		
				}else if (i%2 == 0 && j%2 == 0) {
					//On met un mur incassable une fois sur deux
					tbl[i][j] = MapObjects.UWALL;
				}
			}
		}
		map.setGrid(tbl);
		return map;
	}

	public Grid setBasicLvlMap(int width, int height, int cpt) {
		/* On prend width+2 et height+2 car on encercle la map de mur incassable
		 * Il est donc n�c�ssaire de prendre une marge pour avoir la bonne zone 
		 * disponible pass� en param�tre
		 * */
		Grid map = new Grid(width+2, height+2);
		map = setEmptyMap(width, height); //on met les mur incassable
		MapObjects [][] tbl = map.getGrid();
		Coordinate [] coo = new Coordinate[cpt];
		int x = (int) Math.round(Math.random()*12);
		int y = (int) Math.round(Math.random()*10);
		int temp = 0;
		System.out.println(cpt);
		//On cr�er un tableau comportant les coordonn�es des murs cassable � disposer sur la map
		while (temp < cpt) {
			Coordinate cootemp = new Coordinate(x,y);
			if ((x != 0 && x != width+1 && y != 0 && y != height+1) && (x%2 !=0 || y%2 != 0) && !cootemp.isIn(coo)) {
				coo[temp] = new Coordinate(x,y);
				temp++;
			}
			x = (int) Math.round(Math.random()*12);
			y = (int) Math.round(Math.random()*10);
		}
		
		//On met les murs cassable dans la map
		for (int i = 0 ; i < coo.length ; i++) {
			tbl[coo[i].getX()][coo[i].getY()] = MapObjects.BWALL;
		}
		
		map.setGrid(tbl);
		return map;
	}
	public Grid setLvlMap(int width, int height, int lvl) {
		switch (lvl) {
		case 0:
			return setEmptyMap(width, height);
		case 1:
			return setBasicLvlMap(width, height, 20);
		case 2:
			return setBasicLvlMap(width, height, 30);
		case 3 :
			return setBasicLvlMap(width, height, 40);
		default :
			return setEmptyMap(width, height);
		}
	}
	

}
