package bomberballagain;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class MainGame extends StateBasedGame{ 	
//The game has multiple states(screens), when playing and when we're not
	
	public static final String gamename = "Bomberball";
	public static final int menu = 0;//the state of the game where we are on the menu
	public static final int playing = 1;//the state of the game where we are playing
	//later we're gonna have 2 classes, menu class and play class
	
	public MainGame(String gamename) {
		super(gamename);//constructor
		this.addState(new Menu(menu));
		this.addState(new Playing(playing));	
	}
	//this method demonstrates what kind of screens we're gonna need
	public void initStatesList(GameContainer arg0) throws SlickException {
		this.getState(menu).init(arg0, this);//the home page
		this.getState(playing).init(arg0, this);//the play screen
		this.enterState(menu);//the first screen that we want to show to the user is the menu
	}
	
	public static void main(String[] args) throws SlickException{
		AppGameContainer app;
		//Grid map = new Grid(13,11);
		try {
			app = new AppGameContainer(new MainGame(gamename));
			app.setDisplayMode(1000, 650, false); //(length, width, we don't want a full screen)
			app.setShowFPS(false);
			app.start(); //start the window
		}catch(SlickException e) {
			e.printStackTrace();
		}
	}
	
	
}
