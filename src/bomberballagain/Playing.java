package bomberballagain;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class Playing implements GameState {

	//Image sprite; 
	Animation movingUp, movingDown, movingLeft, movingRight;
    Animation player;
    int[] duration = {200, 200};//how long the animation will last for because we have 2 images
	float playerX = -315; //the strating location
	float playerY = -462; //the strating location
	float cameraX;
    float cameraY;
    float screenWidth;
    float screenHeight;

	private static final float SPEED = 0.1f;
	private Grid grid = new Grid(13,11);
	private Map map = new Map("playMenu", grid);
	
	
	public Playing(int state) {
	}
	
	public void init(GameContainer gc, StateBasedGame bg) throws SlickException {	
		
		//sprite= new Image("images/sprite.png");
		
		map.init(gc);
		
		//Collision map : 
		Globales.blocked = new boolean[grid.getWidth()*500][grid.getHeight()*500];
        for (int x = 0; x < grid.getWidth()*500; x++) {
            for (int y = 0; y < grid.getHeight()*500; y++) {
            	boolean value = false ;
                if (value == true) {
                	Globales.blocked[x][y] = true;
                }
            }
        }
		
		Image[] walkUp = {
				new Image("images/boy_up_1.png"),
	            new Image("images/boy_up_2.png"),
	    };
	    Image[] walkDown = {
	            new Image("images/boy_down_1.png"),
	            new Image("images/boy_down_2.png"),
	    };
	    Image[] walkLeft = {
	            new Image("images/boy_left_1.png"),
	            new Image("images/boy_left_2.png"),
	    };
	    Image[] walkRight = {
	            new Image("images/boy_right_1.png"),
	            new Image("images/boy_right_2.png"),
	    };
		
		movingUp = new Animation(walkUp, duration, false);
        movingDown = new Animation(walkDown, duration, false);
        movingLeft = new Animation(walkLeft, duration, false);
        movingRight = new Animation(walkRight, duration, false);

        player = movingDown;
	}
	
	//the method that draws things on the screen
	public void render(GameContainer gc, StateBasedGame bg, Graphics g) throws SlickException {

		map.render(gc, g);
		screenWidth = gc.getWidth();
        screenHeight = gc.getHeight();
        
        cameraX = (screenWidth / 2) - (playerX / 2);
        cameraY = (screenHeight / 2) - (playerY / 2);

        player.draw(cameraX, cameraY);

        g.drawString("X: " + playerX + "\nY: " + playerY, 520, 20); //draw on the screen the position of the player
        g.resetTransform();
	}
	
	// bonus: 
	//Afficher les bonus dans des cases différentes vides random after chaque x secondes ! 
	
	//update the images on the screen
	public void update(GameContainer gc, StateBasedGame bg, int delta) throws SlickException {
		
		//the keyboard
        Input input = gc.getInput();
        boolean capturedInput = false;
        float yChange = -462, xChange=-315;
        

        if (input.isKeyDown(Input.KEY_UP) || input.isKeyDown(Input.KEY_W)) {
            player = movingUp;
            //yChange += delta * SPEED;
            playerY += delta * SPEED;
            capturedInput = true;
            player.update(delta);
        } else if (input.isKeyDown(Input.KEY_DOWN) || input.isKeyDown(Input.KEY_S)) {
            player = movingDown;
            //yChange -= delta * SPEED;
            playerY -= delta * SPEED;
            capturedInput = true;
            player.update(delta);
        } else if (input.isKeyDown(Input.KEY_LEFT) || input.isKeyDown(Input.KEY_A)) {
            player = movingLeft;
            //xChange += delta * SPEED;
            capturedInput = true;
            playerX += delta * SPEED;
            player.update(delta);
        } else if (input.isKeyDown(Input.KEY_RIGHT) || input.isKeyDown(Input.KEY_D)) {
            player = movingRight;
            //xChange -= delta * SPEED;
            capturedInput = true;
            playerX -= delta * SPEED;
            player.update(delta);
        }
        /*if (capturedInput == true && !(Globales.blocked(playerX + xChange, playerY + yChange ))) {
        	playerX += xChange;
            playerY += yChange;
        	player.update(delta);
        }*/
		
	}
	
	public int getID() {
		return 1;//the id of the playing screen is 1
	}

	@Override
	public void mouseClicked(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseWheelMoved(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void inputEnded() {
		// TODO Auto-generated method stub

	}

	@Override
	public void inputStarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isAcceptingInput() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setInput(Input arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(int arg0, char arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(int arg0, char arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void controllerButtonPressed(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void controllerButtonReleased(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void controllerDownPressed(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void controllerDownReleased(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void controllerLeftPressed(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void controllerLeftReleased(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void controllerRightPressed(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void controllerRightReleased(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void controllerUpPressed(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void controllerUpReleased(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void enter(GameContainer arg0, StateBasedGame arg1) throws SlickException {
		// TODO Auto-generated method stub

	}


	@Override
	public void leave(GameContainer arg0, StateBasedGame arg1) throws SlickException {
		// TODO Auto-generated method stub

	}


}
