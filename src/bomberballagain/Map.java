package bomberballagain;


import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;




public class Map extends BasicGame{
	private Grid map;
	
	public Map(String title, Grid map) {
		super(title);
		this.setMap(map);
	}
	
	public Grid getMap() {return this.map;}
	public void setMap(Grid map) {this.map = map;}
	
	/*public static void main(String[] args) throws SlickException{
		Grid map = new Grid(13,11);
		
		AppGameContainer app = new AppGameContainer(new Map("Map",map));
		app.setDisplayMode(1000,650, false);
		app.setShowFPS(false);
		app.start();
	}*/
	
	@Override
	public void init(GameContainer container) throws SlickException {
		this.map = this.map.setLvlMap(13, 11, 3);
	}

	@Override
	public void render(GameContainer container, Graphics g) throws SlickException {
		//map = map.setEmptyMap(13, 11);
		drawMap(container, g, this.map);
		
	}
	
	@Override
	public void update(GameContainer container, int delta) throws SlickException {
	}
	
	public void drawMap(GameContainer container, Graphics g, Grid map) throws SlickException {
		/*
		 * TAILLE D'UNE CASE : 30
		 * */
		int width = map.getWidth();
		int height = map.getHeight();
		//Cadre de la map
		Rectangle r1 = new Rectangle(1, 1, 749, 649);
		g.setColor(new Color(255,255,255));
		g.draw(r1);
		
		//On affiche la map
		for (int i = 0 ; i < width ; i++) {
			for (int j = 0 ; j < height ; j++) {
				if (map.getGrid()[i][j] == MapObjects.UWALL) {
					Image Uwall = new Image("images/bloc50.jpg");
					Uwall.draw(i*50,j*50);
				}else if (map.getGrid()[i][j] == MapObjects.BWALL) {
					Image Bwall = new Image("images/mur50.jpg");
					Bwall.draw(i*50,j*50);
				}
			}
		}
		
	}

}
