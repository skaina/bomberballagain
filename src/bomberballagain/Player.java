package bomberballagain;


import org.newdawn.slick.*;
import org.newdawn.slick.state.*;


public class Player {
	
	Map gp;
	KeyHandler KeyH;
	entity et = new entity();
	
	public Player(Map gp, KeyHandler KeyH) {
		this.gp = gp;
		this.KeyH = KeyH;
		
		setDefaultValues();
	}
	
	public void setDefaultValues() { //set default values of players
		et.x = 100;
		et.y = 100;
		et.speed = 4;
	}
	public void update() {
		if(KeyH.upPressed == true) {
			et.y -= et.speed; //because in JAVA, the uppder left corner is (0,0) -> (X values increases to the right , Y values increases as they go down) 
		}
		else if(KeyH.downPressed == true) {
			et.y += et.speed;
		}
		else if(KeyH.rightPressed == true) {
			et.x += et.speed;
		}
		else if(KeyH.leftPressed == true) {
			et.x -= et.speed;
		}
	}
	public void draw(Graphics g2) {
		//g2.setColor(Color.white);
		//g2.fillRect(et.x, et.y, 30 , 30);//draw a rectangle that represents a character on the screen //100, 100 represents (X,Y) coordinates of the character
		//Rectangle r = new Rectangle(30, 30, et.x, et.y);
		g2.setColor(new Color(255,255,255));
		//g2.draw(r);
	}

}

