package bomberballagain;

public class Coordinate {
	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	private int x;
	private int y;
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	public boolean isIn(Coordinate[] tbl) {
		int i = 0;
		while (i < tbl.length && tbl[i] != this) {
			i++;
		}
		return i != tbl.length;
	}
	@Override
	public String toString() {
		return "Coordinate [x=" + x + ", y=" + y + "]";
	}
	
}

